# ejericicio-propuesto
---------------TALLER - GRUPO 5-----------


Con el formulario que esta previamente hecho. Deberán validar con JS los inputs del formulario con uso de expresiones regulares:
  - Eliminar caracteres o texto no validos como por ejemplo: eliminar texto de un input de numero de telefono.
  - Antes de enviar el formulario validar que todos los campos esten llenos.
  - Mostrar por Alert o imprimiendo en pantalla si el usuario cometió un error o si se envio el formulario.


Pagina para obtener expresiones regulares: http://regexr.com/
